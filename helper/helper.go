package helper

import (
	"log"
	"strings"

	"github.com/pborman/uuid"
)

// GenerateOptimizedUUID - generates optimized UUID v1 for MySQL
func GenerateOptimizedUUID() string {
	unoptimizedID := uuid.New()
	arr := strings.Split(unoptimizedID, "-")
	optimizedID := arr[2] + arr[1] + arr[0] + arr[3] + arr[4]
	log.Println("OpID:", optimizedID)
	return optimizedID
}
