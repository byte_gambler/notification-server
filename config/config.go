package config

import (
	"os"
	"github.com/kardianos/osext"
	"io/ioutil"
	"log"
	"encoding/json"
	"fmt"
)

var cfg *Config

type (
	Config struct {
		ProjectName      string         `json:"projectName"`
		Environment      string         `json:"environment"`
		Port             string         `json:"port"`
		FcmServerKey     string         `json:"fcmServerKey"`
		DatabaseSettings DatabaseConfig `json:"databaseSettings"`
	}

	// DatabaseConfig -
	DatabaseConfig struct {
		Host     string `json:"host" printable:"Host"`
		Port     string `json:"port" printable:"Port"`
		User     string `json:"user" printable:"User"`
		Password string `json:"password"`
		Database string `json:"database" printable:"Database"`
	}
)

// ReadConfig - read config file
func ReadConfig() (*Config, error) {
	if cfg != nil {
		return cfg, nil
	}

	cfg = new(Config)
	configFilePath := os.Getenv("NOTIFICATION_SERVER_CONFIG_FILE_PATH")
	if len(configFilePath) == 0 {
		folderPath, _ := osext.ExecutableFolder()
		configFilePath = folderPath + "/config.json"
	}
	b, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		log.Println("Failed to open config.json. ", err)
		return cfg, err
	}

	err = json.Unmarshal(b, &cfg)
	if err != nil {
		fmt.Print("bad json ", err)
		return cfg, err
	}

	return cfg, nil
}
