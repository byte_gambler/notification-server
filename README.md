# README #

### How do I get set up? ###

* This project requires the Golang installed on your machine(my Go version 1.8) and GOPATH properly configured.
* Installations steps
* mkdir -p $GOPATH/src/bitbucket.org/
* cd $GOPATH/src/bitbucket.org/
* Clone or copy the notifcation-server folder here.
* cd notification-server
* Configure the your database password in config.json
* create mysql database notificationdb. 
* Run following commands for dependencies and building the project.
* go get -v
* go build -v
* Start the notification server
* ./notification-server
* Check home URL:  http://localhost:9001/api/v1/ on your browser.
### Who do I talk to? ###

* Email me at knowabhi23@gmail.com