package router

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/notification-server/controller"
)

func Init() *gin.Engine {
	router := gin.Default()
	router.LoadHTMLGlob("static/templates/*")

	//	router.GET("/index", controller.IndexHtml)
	//  router.LoadHTMLFiles("/var/www/api-mobility/api/bin/public/index.html")
	//	router.GET("/tick", gin.WrapH(controller.TickStream()))
	//	router.GET("/stream/v1/track", gin.WrapH(controller.TrackStream()))
	v1 := router.Group("/api/v1")
	{
		v1.GET("/test", controller.Test)
		v1.GET("/", controller.LoadHomePage)
		v1.GET("/get/notifications", controller.GetNotifications)
		v1.POST("/update/firebase", controller.UpdateFirebaseId)
		v1.POST("/mark/notifications/read", controller.MarkNotificationsRead)
		v1.POST("/notifications/send", controller.SendNotifications)
	}
	v1.Static("/static", "static")
	return router
}
