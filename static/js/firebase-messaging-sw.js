

// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');

var config = {
    apiKey: "AIzaSyCX8Q5ZaullMXtpfoPRSmsqCKhE2-YNdHY",
    authDomain: "myfirebaseproject-a7d42.firebaseapp.com",
    databaseURL: "https://myfirebaseproject-a7d42.firebaseio.com",
    projectId: "myfirebaseproject-a7d42",
    storageBucket: "myfirebaseproject-a7d42.appspot.com",
    messagingSenderId: "347853818455"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();

