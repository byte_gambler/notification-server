package models

import (
	"bitbucket.org/notification-server/helper"
)

type Firebase struct {
	Id       string `json:"id" gorm:"primary_key"`
	UserId   string `json:"userId"`
	UserType int    `json:"userType"`
	FcmId    string `json:"fcmId"`
	Timestamp
}

func (f *Firebase) Create() error {
	f.Id = helper.GenerateOptimizedUUID()
	return db.Create(f).Error
}

func (f *Firebase) Update() error {
	return db.Save(f).Error
}

type UpdateFirebaseIdInput struct {
	FCMId    string `json:"fCMId"`
}

func UpdateFirebaseId(input *UpdateFirebaseIdInput, userType int, userId string) (bool, error) {
	f := new(Firebase)
	f.UserId = userId
	f.UserType = userType
	f.FcmId = input.FCMId
	err := f.Create()
	if err != nil {
		return false, err
	}

	return true, nil
}

func GetAllFirebaseIds() ([]string, error) {
        fbs := new([]Firebase)
        err := db.Find(fbs).Error
        if err != nil {
                return nil, err
        }

        ids := new([]string)
        for _, f := range *fbs {
                *ids = append(*ids, f.FcmId)
        }

        return *ids, err
}

