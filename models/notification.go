package models

import (
	"github.com/NaySoftware/go-fcm"
	"fmt"
	"time"
	"github.com/Sirupsen/logrus"
	"bitbucket.org/notification-server/config"
)

type Notification struct {
	Id int64 `json:"id" gorm:"primary_key;AUTO_INCREMENT"`

	UserId   int64 `json:"userId"`
	Title    string `json:"title"`
	Body     string `json:"body"`
	Icon     string `json:"icon"`
	IsRead     bool   `json:"is_read"`
	Type     string `json:"type"`
	UserType int    `json:"userType"`

	CreatedTime int64 `json:"createdTime"`

	Timestamp
}

func (n *Notification) Create() error {
	n.CreatedTime = time.Now().Unix()
	return db.Create(n).Error
}

type GetNotificationsInput struct {
	PageNo   *int `json:"pageNo"`
	PageSize *int `json:"pageSize"`
}

type GetNotificationsOutput struct {
	Notifications []Notification `json:"notifications"`
}

func GetNotifications() (*[]Notification, error) {
	notifications := new([]Notification)
	err := db.Order("is_read").Find(&notifications).Error
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return notifications, nil
}

type MarkNotificationReadInput struct {
	Id []int64 `json:"id"`
}

func (n *Notification) Update() error {
	err := db.Save(n).Error
	if err != nil {
		return err
	}
	return nil
}

func MarkNotificationRead(input *MarkNotificationReadInput) (bool, error) {
	var ns Notification
	err := db.Model(&ns).Where("is_read = ?", false).Update("is_read", true).Error
	if err != nil {
		return false, err
	}
	return true, nil
}

func SendNotifications(ids []string) {
	cfg, err := config.ReadConfig()
	if err != nil {
		panic(err)
	}
	c := fcm.NewFcmClient(cfg.FcmServerKey)
	data := map[string]string{
		"type": "sample_notification",
	}
	c.NewFcmRegIdsMsg(ids, data)
	payload := new(fcm.NotificationPayload)
	payload.Title = "This is a test notification"
	payload.Body = "Abhishek tagged you"
	payload.Icon = "default"
	c.SetNotificationPayload(payload)

	status, err := c.Send()
	if err == nil {
		status.PrintResults()
		notification := new(Notification)
		notification.Type = data["type"]
		notification.Title = payload.Title
		notification.Body = payload.Body
		notification.Icon = payload.Icon
		notification.UserId = 34
		notification.UserType = 1
		notification.IsRead = false
		err := notification.Create()
		if err != nil {
			fmt.Println(err)
		}
	} else {
		fmt.Println(err)
	}
}
