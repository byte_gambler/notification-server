package models

import "time"

// Model - this will be base model for all models
// coonected to mysql
type Timestamp struct {
	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt time.Time  `json:"updatedAt"`
	DeletedAt *time.Time `json:"deletedAt"`
}
