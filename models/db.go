package models

import (
	"github.com/jinzhu/gorm"

	"log"
	"bitbucket.org/notification-server/config"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var db *gorm.DB

// ConnectDB
func ConnectDB() (*gorm.DB, error) {
	cfg, err := config.ReadConfig()
	if err != nil {
		panic(err)
	}
	connString := cfg.DatabaseSettings.User + ":" + cfg.DatabaseSettings.Password + "@tcp(" + cfg.DatabaseSettings.Host + ":" + cfg.DatabaseSettings.Port + ")/" + cfg.DatabaseSettings.Database
	db, err = gorm.Open("mysql", connString+"?parseTime=true")
	if err != nil {
		log.Panic(err)
		return nil, err
	}

	db.SingularTable(true)
	db = db.LogMode(true)

	if err := db.DB().Ping(); err != nil {
		panic(err)
	}

	log.Println("Connected to db...")

	//DbDropTables()
	DbMigrate()

	//GenerateData()
	return db, nil
}

func DbMigrate() {
	db.AutoMigrate(Notification{})
	db.AutoMigrate(Firebase{})

	err := db.Model(&Firebase{}).AddUniqueIndex("firebase_key", "fcm_id").Error
	if err != nil {
		panic(err)
	}
}
