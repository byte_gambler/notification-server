package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"bitbucket.org/notification-server/models"
)

func AbortWithError(c *gin.Context, status int, err error) {
	msg := struct {
		Msg string `json:"msg"`
	}{
		Msg: err.Error(),
	}
	c.JSON(status, msg)
	c.AbortWithError(status, err)
}

func Test(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"msg": "This is just a sample message"})
}

func LoadHomePage(c *gin.Context) {
	// Call the HTML method of the Context to render a template
	c.HTML(
		// Set the HTTP status to 200 (OK)
		http.StatusOK,
		// Use the index.html template
		"index.html",
		// Pass the data that the page uses (in this case, 'title')
		gin.H{
			"title": "Home Page",
		},
	)
}

func GetNotifications(c *gin.Context) {
	resp := new([]models.Notification)
	resp, err := models.GetNotifications()
	if err != nil {
		AbortWithError(c, 400, err)
		return
	}
	c.JSON(200, resp)
}

func UpdateFirebaseId(c *gin.Context) {
	input := new(models.UpdateFirebaseIdInput)
	if err := c.BindJSON(&input); err != nil {
		AbortWithError(c, 400, err)
		return
	}
	output, err := models.UpdateFirebaseId(input, 0, "DUMMY-USER")
	response := struct {
		Msg string `json:"msg"`
	}{}
	if output == true  && err == nil {
		response.Msg = "Firebase Id updated successfully"
		c.JSON(200, response)
	} else {
		response.Msg = "Error while updating Firebase Id"
		AbortWithError(c, 400, err)
	}
}

func MarkNotificationsRead(c *gin.Context) {
	input := new(models.MarkNotificationReadInput)
	if err := c.BindJSON(&input); err != nil {
		AbortWithError(c, 400, err)
		return
	}
	response := struct {
		Msg string `json:"msg"`
	}{}
	_, err := models.MarkNotificationRead(input)
	if err != nil {
		response.Msg = "Error while updating notifications"
	}
}

func SendNotifications(c *gin.Context) {
	ids, err := models.GetAllFirebaseIds()
	if err != nil {
		AbortWithError(c, 400, err)
		return
	}
	models.SendNotifications(ids)
	c.JSON(200, gin.H{"msg" : "Notifications sent successfully"})
}