package main

import (
	"log"
	"bitbucket.org/notification-server/config"
	"bitbucket.org/notification-server/models"
	"bitbucket.org/notification-server/router"
	"net/http"
	"bitbucket.org/notification-server/crons"
)

func main() {
	cfg, err := config.ReadConfig()
	if err != nil {
		log.Panic(err)
	}
	cssHandler := http.FileServer(http.Dir("./static/css/"))
	http.Handle("/css/", http.StripPrefix("/css/", cssHandler))

	db, err := models.ConnectDB()
	if err != nil {
		log.Fatal("Failed to connect db.", err)
	}

	crons.StartNotificationGenerationCron()
	defer db.Close()
	r := router.Init()
	r.Run(cfg.Port)
}
