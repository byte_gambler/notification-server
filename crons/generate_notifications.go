package crons

import (
	"bitbucket.org/notification-server/models"
	"github.com/robfig/cron"
	"github.com/Sirupsen/logrus"
)

func StartNotificationGenerationCron() {
	cron := cron.New()
	cron.AddFunc("@every 10s", SendNotificaitionsCron)
	cron.Start()
}

func SendNotificaitionsCron() {
	ids, err := models.GetAllFirebaseIds()
	if err != nil {
		logrus.Error(err)
		return
	}
	models.SendNotifications(ids)
}

